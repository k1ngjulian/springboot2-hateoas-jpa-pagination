# README #

Accompanying source code for blog entry at https://tech.asimio.net/2020/04/16/Adding-HAL-pagination-links-to-RESTful-applications-using-Spring-HATEOAS.html


### Requirements ###

* Java 8+
* Maven 3.2.x+

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl -v "http://localhost:8800/api/films?page=2&size=10"
curl -v -H "Accept: application/hal+json" "http://localhost:8800/api/films?page=2&size=10"
curl -v "http://localhost:8800/api/films/{id}"   (1, for instance)
```
should result in successful responses.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero