package com.asimio.demo.rest;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.asimio.demo.domain.Film;
import com.asimio.demo.rest.mapper.FilmResourceMapper;
import com.asimio.demo.rest.model.FilmResource;
import com.asimio.demo.service.DvdRentalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
        FilmResourceMapper.class,
        ServletUriComponentsBuilder.class,
        UriComponentsBuilder.class,
        UriComponents.class,
        Link.class
})
public class FilmControllerTest {

    @Mock
    private DvdRentalService mockDvdRentalService;

    @Mock
    private ResourceAssembler<Film, FilmResource> mockFilmResourceAssembler;

    private FilmController controller;

    @Before
    public void setup() {
        this.controller = new FilmController(this.mockDvdRentalService, this.mockFilmResourceAssembler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldRetrieveFilms() {
        // Given
        PowerMockito.mockStatic(ServletUriComponentsBuilder.class);
        PowerMockito.mockStatic(UriComponentsBuilder.class);
        PowerMockito.mockStatic(UriComponents.class);
        PowerMockito.mockStatic(Link.class);

        int pageNo = 3;
        int pageSize = 5;
        Pageable pageRequest = PageRequest.of(pageNo, pageSize);
        Page<Film> filmsPage = Mockito.mock(Page.class);
        ServletUriComponentsBuilder mockServletUriComponentsBuilder = Mockito.mock(ServletUriComponentsBuilder.class);
        UriComponentsBuilder mockUriComponentBuilder = Mockito.mock(UriComponentsBuilder.class); 
        UriComponents mockUriComponents = Mockito.mock(UriComponents.class);
        PagedResources<FilmResource> mockPagedResources = Mockito.mock(PagedResources.class);
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
        PagedResourcesAssembler<Film> mockPagedResourcesAssembler = Mockito.mock(PagedResourcesAssembler.class);

        Mockito.when(ServletUriComponentsBuilder.fromCurrentRequest())
            .thenReturn(mockServletUriComponentsBuilder);
        Mockito.when(mockServletUriComponentsBuilder.build())
            .thenReturn(mockUriComponents);
        Mockito.when(mockUriComponents.toUriString())
            .thenReturn("/api/films");
        Mockito.when(UriComponentsBuilder.fromUriString("/api/films"))
            .thenReturn(mockUriComponentBuilder);
        Mockito.when(mockUriComponentBuilder.build())
            .thenReturn(mockUriComponents);
        Mockito.when(mockUriComponents.getQueryParams())
            .thenReturn(multiValueMap);
        Mockito.when(this.mockDvdRentalService.retrieveFilms(Mockito.eq(pageRequest)))
            .thenReturn(filmsPage);
        Mockito.when(mockPagedResourcesAssembler.toResource(Mockito.eq(filmsPage), Mockito.eq(this.mockFilmResourceAssembler), Mockito.any(Link.class)))
            .thenReturn(mockPagedResources);

        // When
        ResponseEntity<PagedResources<FilmResource>> actualResponse = this.controller.retrieveFilms(pageRequest, mockPagedResourcesAssembler);

        // Then
        Assert.assertThat(actualResponse.getStatusCode(), Matchers.equalTo(HttpStatus.OK));
        Assert.assertThat(actualResponse.getBody(), Matchers.is(mockPagedResources));

        Mockito.verify(mockServletUriComponentsBuilder, Mockito.times(1)).build();
        Mockito.verify(mockUriComponents, Mockito.times(1)).toUriString();
        Mockito.verify(mockUriComponentBuilder, Mockito.times(1)).build();
        Mockito.verify(mockUriComponents, Mockito.times(1)).getQueryParams();
        Mockito.verify(this.mockDvdRentalService, Mockito.times(1)).retrieveFilms(Mockito.eq(pageRequest));
        Mockito.verify(mockPagedResourcesAssembler, Mockito.times(1))
            .toResource(
                    Mockito.eq(filmsPage),
                    Mockito.eq(this.mockFilmResourceAssembler),
                    Mockito.any(Link.class)
            );

        Mockito.verifyNoMoreInteractions(mockServletUriComponentsBuilder, mockUriComponents, mockUriComponentBuilder,
                mockPagedResourcesAssembler, this.mockDvdRentalService);
    }
}